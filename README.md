#Intro :
 ##Real-time charts using Angular 6, D3, and Socket.IO

###client Side Dependencies
####1."@angular/animations": "^6.1.0"
####2."@angular/common": "^6.1.0"
####3."@angular/compiler": "^6.1.0"
####4."@angular/core": "^6.1.0"
####5."@angular/forms": "^6.1.0"
####6."@angular/http": "^6.1.0"
####7."@angular/platform-browser": "^6.1.0"
####8."@angular/platform-browser-dynamic": "^6.1.0"
####9."@angular/router": "^6.1.0"
####10."core-js": "^2.5.4"
####11."d3": "^5.7.0"
####12."rxjs": "^6.0.0"
####13."socket.io-client": "^2.1.1"
####14."zone.js": "~0.8.26"

###Server Side Dependencies :

####1."express": "^4.16.3",
####2."moment": "^2.22.2",
####3."socket.io": "^2.1.1"

###Screenshot:

![Example](./resources/Screenshot1.png "Example1")

###Article

[Link] (https://auth0.com/blog/real-time-charts-using-angular-d3-and-socket-io/)
